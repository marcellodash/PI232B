# PI232B: RS232 Level Shifter for Raspberry Pi Model B
## by Mark J. Blair <nf6x@nf6x.net>

This board is an RS232 level shifter for the Raspberry Pi Model B,
which is the version with a 26-pin J1 next to an 8-pin J5. It is not
intended for use with any other versions of the Raspberry Pi board.
The external RS232 connection is via an RJ45 jack wired to the [Yost
serial port wiring standard](http://yost.com/computers/RJ45-serial/).

![](pics/PI232B-render01.png?raw=true) 

![](pics/PI232B-render02.png?raw=true) 

It provides full hardware handshaking, with the following pin
assignments:

GPIO   | Function | Dir | RJ45 Pin
-------|----------|-----|----------
GPIO14 | TXD      | out | 6
GPIO15 | RXD      | in  | 3
GPIO17 | DCD/DSR  | in  | 2
GPIO18 | DTR      | out | 7
GPIO30 | CTS      | in  | 1
GPIO31 | RTS      | out | 8
GND    | GND      |     | 4 
GND    | GND      |     | 5 

0 ohm series resistors allow rewiring as necessary.

This design is open source with no license restrictions and no
warranty. Use at your own risk.
